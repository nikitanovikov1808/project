//
//  SessionManager.h
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@class User;


@interface SessionManager : AFHTTPSessionManager

+ (SessionManager*) sharedManager;
- (void) authorizeUser:(void(^)(User* user)) completion;
- (void) exit;

- (void) authorizeUserInWebWiew:(void(^)(User* user)) completion;

- (void) getFriendsWithOffset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^)(NSArray* friends)) success
                    onFailure:(void(^)(NSError* error)) failure;

- (void)  getDialogWithFriend:(NSString *)friendID
                       offset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^)(NSArray* friends)) success
                    onFailure:(void(^)(NSError* error)) failure;

- (void)  searchFriend:(NSString *)friend
                    onSuccess:(void(^)(NSArray* friends)) success
                    onFailure:(void(^)(NSError* error)) failure;

- (void)  sendMessage:(NSString *) message
             toFriend:(NSString *)friendID
            onSuccess:(void(^)(NSString* succes)) success
            onFailure:(void(^)(NSError* error)) failure;

- (void) getChatsWithOffset:(NSInteger) offset
                      count:(NSInteger) count
                  onSuccess:(void(^)(NSDictionary* chatsAndUsers)) success
                  onFailure:(void(^)(NSError* error)) failure;

@end
