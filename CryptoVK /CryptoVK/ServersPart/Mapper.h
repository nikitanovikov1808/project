//
//  Mapper.h
//  CryptoVK
//
//  Created by 1 on 07.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Mapper : NSObject

-(User *) userFromResponse:(NSDictionary *) responseObject;

-(NSArray *) usersFromResponse:(NSDictionary *) responseObject;

-(NSArray *) friendsFromResponse:(NSDictionary *) responseObject;

-(NSDictionary *) userIDsandChatsFromResponse:(NSDictionary *) responseObject;

-(NSArray *) dialogWithFriendFromResponse:(NSDictionary *) responseObject;



@end
