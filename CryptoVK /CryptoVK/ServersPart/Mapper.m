//
//  Mapper.m
//  CryptoVK
//
//  Created by 1 on 07.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "Mapper.h"
#import "EncryptionAlgorithm.h"

static NSString *prefixForEncryptionMessage = @"encryption message:";
static NSString *prefixForIDUser = @"<-userID,message->";

@implementation Mapper

-(User *) userFromResponse:(NSDictionary *) responseObject{
    NSArray* dictsArray = [responseObject objectForKey:@"response"];
    
    if ([dictsArray count] > 0) {
        User* user = [[User alloc] initWithServerResponse:[dictsArray firstObject]];
        return user;
    }
    else
        return nil;
}

-(NSArray *) usersFromResponse:(NSDictionary *) responseObject{
    NSMutableArray *users=[NSMutableArray array];
    NSArray* dictsArray = [responseObject objectForKey:@"response"];
    for (NSDictionary *userInfo in dictsArray){
        User* user = [[User alloc] initWithServerResponse:userInfo];
        [users addObject:user];
    }
    return users;
}

-(NSArray *) friendsFromResponse:(NSDictionary *) responseObject{
    NSArray* dictsArray = [responseObject objectForKey:@"response"];
    NSMutableArray* objectsArray = [NSMutableArray array];
    for (id obj in dictsArray) {
        if ([obj isKindOfClass:[NSDictionary class]]){
            NSDictionary *dict=obj;
            User* user = [[User alloc] initWithServerResponse:dict];
            [objectsArray addObject:user];
        }
    }
    return objectsArray;

}

-(NSDictionary *) userIDsandChatsFromResponse:(NSDictionary *) responseObject{
    NSMutableArray *chats=[NSMutableArray new];
    NSMutableString *userIDs=[NSMutableString new];
    NSArray* dictsArray = [responseObject objectForKey:@"response"];
    for (id obj in dictsArray) {
        if ([obj isKindOfClass:[NSDictionary class]]){
            NSDictionary *dict=obj;
            NSString *uid=[dict objectForKey:@"uid"];
            if (![[dict objectForKey:@"body"]isEqualToString:@""] &&([[dict objectForKey:@"title"]isEqualToString:@" ... "]||[[dict objectForKey:@"title"]isEqualToString:@""])&&uid.integerValue>0){
                [userIDs appendFormat:@"%@,",uid];
                [chats addObject:[dict objectForKey:@"body"]];
            }
        }
    }
        NSDictionary *chatsAndUsersIDs=
        [NSDictionary dictionaryWithObjectsAndKeys:
         userIDs,      @"userIDs",
         chats,       @"chats",
         nil];
        return chatsAndUsersIDs;
}

-(NSArray *) dialogWithFriendFromResponse:(NSDictionary *) responseObject{
    NSArray* dictsArray = [responseObject objectForKey:@"response"];
    NSMutableArray* objectsArray = [NSMutableArray array];
    for (id dict in dictsArray) {
        if ([dict isKindOfClass:[NSDictionary class]]){
            NSString *message=[dict objectForKey:@"body"];
            if (![message isEqualToString:@""]){
                message=[message stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
                if ([message hasPrefix:prefixForEncryptionMessage]){
                    EncryptionAlgorithm *cryptoAlgorithm=[EncryptionAlgorithm new];
                    message=[cryptoAlgorithm decryptMessage:[message substringFromIndex:[prefixForEncryptionMessage length]]];
                }
                NSString *idAndMessage=[NSString stringWithFormat:@"%@%@%@",[dict objectForKey:@"from_id"],prefixForIDUser, message];
                [objectsArray addObject:idAndMessage];
            }
        }
    }
    return objectsArray;
}

@end
