//
//  SessionManager.m
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "SessionManager.h"
#import "User.h"
#import "LoginViewController.h"
#import "AccessToken.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Token+CoreDataClass.h"
#import "Password+CoreDataClass.h"
#import "Mapper.h"


@interface SessionManager ()

@property (strong, nonatomic) AccessToken* accessToken;
@property (strong, nonatomic) Mapper *mapper;

@end

@implementation SessionManager

+ (SessionManager*) sharedManager {
    
    static SessionManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SessionManager alloc] initSharedInstance];
    });
    return manager;
}

- (instancetype)initSharedInstance {
    NSURL *baseURL = [NSURL URLWithString:@"https://api.vk.com/method/"];
    self = [super initWithBaseURL:baseURL];
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer new];
        self.responseSerializer = [AFJSONResponseSerializer new];
        self.accessToken=[AccessToken new];
        self.mapper=[Mapper new];
        
    }
    return self;
}


#pragma mark - Authorizing

- (void) authorizeUser:(void(^)(User* user)) completion {
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *request = [Token fetchRequest];
    NSArray <Token*>*tokens=[[context executeFetchRequest:request error:nil] copy];
    Token *token=[tokens firstObject];
    
    if (token  && [token.expirationDate compare:[NSDate date]]){
        self.accessToken.token = token.token;
        self.accessToken.expirationDate = token.expirationDate;
        self.accessToken.userID = token.userID;
        {
            [self getUser:self.accessToken.userID
                onSuccess:^(User *user) {
                    if (completion) {
                        completion(user);
                    }
                }
                onFailure:^(NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (completion) {
                        completion(nil);
                    }
                }];
        }
    }
    else{
        [self authorizeUserInWebWiew:completion];
    }
}

- (void) authorizeUserInWebWiew:(void(^)(User* user)) completion {
    LoginViewController* vc =
    [[LoginViewController alloc] initWithCompletionBlock:^(AccessToken *token) {
       
        [self deleteOldToken];
        [self saveNewToken:token];
        
        if (token) {
            
            [self getUser:self.accessToken.userID
                onSuccess:^(User *user) {
                    if (completion) {
                        completion(user);
                    }
                }
                onFailure:^(NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (completion) {
                        completion(nil);
                    }
                }];
            
        } else if (completion) {
            completion(nil);
        }
        
    }];
    
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    UIViewController* mainVC = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
    [mainVC presentViewController:nav
                         animated:YES
                       completion:nil];
    
}
- (void) exit{
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [self deleteOldToken];
    [self deleteOldPassword];
    self.accessToken=nil;
    exit(0);

}

-(void) deleteOldPassword{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *request = [Password fetchRequest];
    NSArray <Password*> *passwords=[[context executeFetchRequest:request error:nil] copy];
    for (Password *passwordsForDelete in passwords){
        [context deleteObject:passwordsForDelete];
    }
    [context save:nil];
}

- (void) deleteOldToken{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *request = [Token fetchRequest];
    NSArray <Token*>*tokens=[[context executeFetchRequest:request error:nil] copy];
    for (Token *tokenForDelete in tokens){
        [context deleteObject:tokenForDelete];
    }
    [context save:nil];
}

-(void) saveNewToken:(AccessToken *) token{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    Token *tokenForSave=[[Token alloc]initWithContext:context];
    tokenForSave.token=token.token;
    tokenForSave.userID=token.userID;
    tokenForSave.expirationDate=token.expirationDate;
    [context save:nil];
    
    self.accessToken = token;
}


#pragma mark - GET request

- (NSURLSessionDataTask *) getUser:(NSString*) userID
       onSuccess:(void(^)(User* user)) success
       onFailure:(void(^)(NSError* error)) failure {
    if (!userID){
        NSLog(@"ERROR: userID=nil");
        return nil;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,        @"user_ids",
     @"photo_50",   @"fields",
     @"nom",        @"name_case", nil];
    
    return [self GET:@"users.get"
          parameters:params
            progress:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 User *user=[self.mapper userFromResponse:responseObject];
                     if (success) {
                         success(user);
                 } else {
                     if (failure) {
                         NSLog(@"response is empty");
                         failure(nil);
                     }
                 }
             } failure:^(NSURLSessionDataTask *task, NSError *error) {
                 if (failure) {
                     failure(error);
                 }
             }];
}

- (NSURLSessionDataTask *) getUsers:(NSString*) userID
                         onSuccess:(void(^)(NSArray *users)) success
                         onFailure:(void(^)(NSError* error)) failure {
    if (!userID){
        NSLog(@"ERROR: userID=nil");
        return nil;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,        @"user_ids",
     @"photo_50",   @"fields",
     @"nom",        @"name_case", nil];
    
    return [self GET:@"users.get"
          parameters:params
            progress:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 NSArray *users=[self.mapper usersFromResponse:responseObject];
                 if (success) {
                     success(users);
                 } else {
                     if (failure) {
                         NSLog(@"response is empty");
                         failure(nil);
                     }
                 }
             } failure:^(NSURLSessionDataTask *task, NSError *error) {
                 if (failure) {
                     failure(error);
                 }
             }];
}
- (void) getFriendsWithOffset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^)(NSArray* friends)) success
                    onFailure:(void(^)(NSError* error)) failure{
    
    if (!self.accessToken){
        NSLog(@"ERROR: token=nil");
        return;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     self.accessToken.userID,   @"user_id",
     @"hints",                  @"order",
     @(count),                  @"count",
     @(offset),                 @"offset",
     @"photo_50",               @"fields",
     @"nom",                    @"name_case",
    self.accessToken.token,     @"access_token",
     nil];
    
    [self GET:@"friends.get"
     parameters:params
     progress:nil
      success:^(NSURLSessionDataTask * task, id  responseObject) {
          NSArray* friends = [self.mapper friendsFromResponse:responseObject];
         if (success) {
             success(friends);
         }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
         NSLog(@"Error: %@", error);
         if (failure) {
             failure(error);
         }
     }];
}


- (void)  getDialogWithFriend:(NSString *)friendID
                       offset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^)(NSArray* friends)) success
                    onFailure:(void(^)(NSError* error)) failure{
    
    if (!self.accessToken){
        NSLog(@"ERROR: token=nil");
        return;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     @(offset),                 @"offset",
     @(count),                  @"count",
     friendID,                  @"user_id",
     self.accessToken.token,    @"access_token",
     nil];
    
    [self GET:@"messages.getHistory"
   parameters:params
     progress:nil
      success:^(NSURLSessionDataTask * task, id  responseObject) {
          NSArray* friends = [self.mapper dialogWithFriendFromResponse:responseObject];
                  if (success) {
              success(friends);
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"Error: %@", error);
          if (failure) {
              failure(error);
          }
      }];
}

- (void)  searchFriend:(NSString *)friend
             onSuccess:(void(^)(NSArray* friends)) success
             onFailure:(void(^)(NSError* error)) failure{
    if (!self.accessToken){
        NSLog(@"ERROR: token=nil");
        return;
    }
    if (!friend && [friend isEqualToString:@""]){
        NSLog(@"ERROR: can not search nil string");
        return;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     friend,                      @"q",
     @"photo_50",                 @"fields",
     @"nom",                      @"name_case",
     @(0),                        @"offset",
     @(10),                       @"count",
     self.accessToken.token,      @"access_token",
     @(5.65),                     @"version",
     nil];
    
    [self GET:@"friends.search"
   parameters:params
     progress:nil
      success:^(NSURLSessionDataTask * task, id  responseObject) {
          NSArray* friends = [self.mapper friendsFromResponse:responseObject];
          if (success) {
              success(friends);
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"Error: %@", error);
          if (failure) {
              failure(error);
          }
      }];
}

- (void)  sendMessage:(NSString *) message
             toFriend:(NSString *)friendID
            onSuccess:(void(^)(NSString* success)) success
            onFailure:(void(^)(NSError* error)) failure{
    if (!self.accessToken){
        NSLog(@"ERROR: token=nil");
        return;
    }
    if (!friendID && [friendID isEqualToString:@""]){
        NSLog(@"ERROR: friendID should be not nil");
        return;
    }
    if (!message && [message isEqualToString:@""]){
        NSLog(@"ERROR: message should be not nil");
        return;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     friendID,                    @"user_id",
     message,                     @"message",
     self.accessToken.token,      @"access_token",
     @(5.65),                     @"version",
     nil];
    
    [self GET:@"messages.send"
   parameters:params
     progress:nil
      success:^(NSURLSessionDataTask * task, id  responseObject) {
          if (success) {
              success(@"success");
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"Error: %@", error);
          if (failure) {
              failure(error);
          }
      }];
}

- (void) getChatsWithOffset:(NSInteger) offset
                      count:(NSInteger) count
                  onSuccess:(void(^)(NSDictionary* chatsAndUsers)) success
                  onFailure:(void(^)(NSError* error)) failure{
    if (!self.accessToken){
        NSLog(@"ERROR: token=nil");
        return;
    }
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     @(count),                  @"count",
     @(offset),                 @"offset",
     self.accessToken.token,     @"access_token",
     @(5.65),                     @"version",
     nil];
    [self GET:@"messages.getDialogs"
   parameters:params
     progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSDictionary *chatsAndUsersIDs=[self.mapper userIDsandChatsFromResponse:responseObject];
          NSArray *chats=[chatsAndUsersIDs objectForKey:@"chats"];
          NSString *userIDs=[chatsAndUsersIDs objectForKey:@"userIDs"];;
          [self getUsers:userIDs
               onSuccess:^(NSArray *users) {
                   NSDictionary *chatsAndUsers=
                  [NSDictionary dictionaryWithObjectsAndKeys:
                   users,      @"users",
                   chats,       @"chats",
                   nil];
                  if (success){
                      success(chatsAndUsers);
                  }
              }
              onFailure:^(NSError *error) {
                  NSLog(@"Error: %@", error);
              }];
         
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          if (failure) {
              failure(error);
          }
      }];

}




@end
