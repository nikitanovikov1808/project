//
//  ActionHandlerForVKChats.h
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ActionHandlerForVKChats <NSObject>

- (void)didScrollToBottom;


@end
