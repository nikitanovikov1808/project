//
//  VKChatsVCDataSource.m
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKChatsVCDataSource.h"
#import "User.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"


@interface VKChatsVCDataSource()

@property (nonatomic, weak) id  actionHandler;

@end

@implementation VKChatsVCDataSource

- (instancetype)initWithActionHandler:(id)actionHandler {
    self = [super init];
    if (self) {
        _actionHandler = actionHandler;
        _chats=[@[] mutableCopy];
        _users=[@[] mutableCopy];
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.chats) {
        return self.chats.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DialogCellID"
                                                            forIndexPath:indexPath];
    User *user=self.users[indexPath.row];
    NSString *chat=self.chats[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",user.firstName, user.lastName];
    cell.detailTextLabel.text=chat;
    UIImage *noPhotoImage=[UIImage imageNamed:@"noPhotoUserAvatar.jpg"];
    [cell.imageView sd_setImageWithURL:user.imageURL placeholderImage:noPhotoImage];

    return cell;
}

@end
