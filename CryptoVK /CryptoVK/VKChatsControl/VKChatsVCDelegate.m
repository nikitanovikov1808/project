//
//  VKChatsVCDelegate.m
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKChatsVCDelegate.h"
#import "ActionHandlerForVKChats.h"
@interface VKChatsVCDelegate()

@property (nonatomic, weak) id <ActionHandlerForVKChats> actionHandler;

@end

@implementation VKChatsVCDelegate

- (instancetype)initWithActionHandler:(id <ActionHandlerForVKChats>)actionHandler {
    self = [super init];
    if (self) {
        _actionHandler = actionHandler;
    }
    return self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > scrollView.contentSize.height - CGRectGetHeight(scrollView.frame)) {
        [self.actionHandler didScrollToBottom];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
