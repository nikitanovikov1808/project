//
//  VKChatsVCDataSource.h
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VKChatsVCDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, copy) NSMutableArray *chats;
@property (nonatomic, copy) NSMutableArray *users;

- (instancetype)initWithActionHandler:(id) actionHandler;

@end
