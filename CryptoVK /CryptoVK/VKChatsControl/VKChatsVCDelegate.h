//
//  VKChatsVCDelegate.h
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionHandlerForVKChats;

@interface VKChatsVCDelegate : NSObject <UITableViewDelegate>

- (instancetype)initWithActionHandler:(id <ActionHandlerForVKChats>)actionHandler;


@end
