//
//  VKChatsViewController.m
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKChatsViewController.h"
#import "SessionManager.h"
#import "User.h"
#import "VKChatsVCDataSource.h"
#import "VKChatsVCDelegate.h"
#import "ActionHandlerForVKChats.h"
#import "VKDialogViewController.h"

static  NSInteger chatsAndUsersInRequestCount = 30;
static  NSInteger chatsAndUsersInRequestOffset = 0;

@interface VKChatsViewController () <ActionHandlerForVKChats>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) VKChatsVCDataSource *dataSource;
@property (nonatomic, strong) VKChatsVCDelegate *delegate;
@property (assign, nonatomic) BOOL isObtaining;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation VKChatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configurateDelegates];
    self.isObtaining = NO;
    [self authorize];
    
}

-(void) configurateDelegates{
    self.delegate = [[VKChatsVCDelegate alloc] initWithActionHandler:self];
    self.tableView.delegate=self.delegate;
    self.dataSource = [[VKChatsVCDataSource alloc] initWithActionHandler:self];
    self.tableView.dataSource=self.dataSource;
}
- (void) authorize{
    __weak VKChatsViewController *weakSelf = self;
    [[SessionManager sharedManager] authorizeUser:^(User *user) {
        [weakSelf configurateAuthorizedUser:user];
        [weakSelf obtainChatsAndUsers];
    }];
}

- (void) configurateAuthorizedUser:(User *) user{
    __weak VKChatsViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue
                   (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       NSData *data=[NSData dataWithContentsOfURL:user.imageURL];
                       UIImage *image=[UIImage imageWithData:data];
                       UIImageView *imageView=[[UIImageView alloc] initWithImage: image];
                       imageView.frame = CGRectMake(0, 0, 40, 40);
                       imageView.layer.cornerRadius = 20.0;
                       imageView.layer.masksToBounds = YES;
                       dispatch_async(dispatch_get_main_queue(), ^{
                           weakSelf.navigationItem.titleView = imageView;
                       }) ;
                   });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) obtainChatsAndUsers{
    __weak VKChatsViewController *weakSelf = self;
    [self.activityIndicator startAnimating];
    [[SessionManager sharedManager]getChatsWithOffset:chatsAndUsersInRequestOffset
                                                count:chatsAndUsersInRequestCount
                                            onSuccess:^(NSDictionary *chatsAndUsers) {
                                                [weakSelf.dataSource.chats addObjectsFromArray:[chatsAndUsers objectForKey:@"chats"]];
                                                [weakSelf.dataSource.users addObjectsFromArray:[chatsAndUsers objectForKey:@"users"]];
                                                weakSelf.isObtaining = NO;
                                                chatsAndUsersInRequestOffset+=chatsAndUsersInRequestCount;
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [weakSelf.activityIndicator stopAnimating];
                                                    [weakSelf.tableView reloadData];

                                                });
                                            }
                                            onFailure:^(NSError *error) {
                                                NSLog(@"%@",error);
                                            }];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ToDialogFromChats"]) {
        VKDialogViewController *viewController = segue.destinationViewController;
        NSUInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
        User *user = self.dataSource.users[selectedIndex];
        viewController.friend = user;
    }
}

#pragma mark - ActionHandlerForVKChats Implementation
- (void)didScrollToBottom{
    if (self.isObtaining) {
        return;
    }
    if (self.dataSource.chats.count > 0) {
        [self.activityIndicator startAnimating];
        [self obtainChatsAndUsers];
    }
    else
        return;
    self.isObtaining = YES;
}





@end
