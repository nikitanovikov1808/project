//
//  EncryptionAlgorithm.h
//  CryptoVK
//
//  Created by 1 on 29.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptionAlgorithm : NSObject

- (NSString *)  encryptMessage:(NSString *) message;

- (NSString *)  decryptMessage:(NSString *) message;

@end
