//
//  EncryptionAlgorithm.m
//  CryptoVK
//
//  Created by 1 on 29.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "EncryptionAlgorithm.h"

@implementation EncryptionAlgorithm

static NSString *prefixForEncryptionMessage = @"encryption message:";

//параметры линейного конгруэнтного генератора псевдослучайных чисел
static const int a = 106;
static const int c = 1283;
static const int m = 6075;
static  int numbers[m];


-(void) initEncryptionAlgorithm{
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        numbers[0]=1;
        for( int i=1; i<m; i++){
            numbers[i] = (a * numbers[i-1] + c) % 2;
        }    });
    
}

- (NSString *)  encryptMessage:(NSString *) message{
    NSString *encryptionMessage=[self encryptionAlgorithm:message];
    return [NSString stringWithFormat:@"%@%@",prefixForEncryptionMessage,encryptionMessage];
}

- (NSString *)  decryptMessage:(NSString *) message{
    NSString *decryptionMessage=[self encryptionAlgorithm:message];
    return decryptionMessage;

}

//алгоритм шифрования заключается в наложение псевдослучайной последовательности на исходный текст с помощью ХОR
//русский язык не удалось зашифровать, так как кодировка русских символов - двубайтная(?), исходя из этого данный алгоритм не применим к русскому языку
- (NSString *)  encryptionAlgorithm:(NSString *) message{
    if ([message length]>m){
        return message;
    }
    [self initEncryptionAlgorithm];
    unsigned char s;
    NSMutableString *newChar=[NSMutableString new];
    for (int i=0; i<[message length];i++){
        s=(unsigned char)[message characterAtIndex:i]^(unsigned char)numbers[i];
        [newChar appendString:[NSString stringWithFormat:@"%c",s]];
    }
    return newChar;
}


@end
