//
//  InputPasswordViewController.m
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "InputPasswordViewController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Password+CoreDataClass.h"

@interface InputPasswordViewController () 
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation InputPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.passwordTextField becomeFirstResponder];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)didChangeEditing:(UITextField *)sender {
    if ([sender.text length]==4){
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        NSFetchRequest *request = [Password fetchRequest];
        NSArray <Password*> *passwords=[[context executeFetchRequest:request error:nil] copy];
        Password *password=[passwords firstObject];

        if ([sender.text isEqualToString:password.numberText]){
             [self performSegueWithIdentifier:@"succesFromPassword" sender:nil];
        }
        else{
            self.passwordTextField.text=nil;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Неверный пароль! Попробуйте еще раз"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }

    
}

@end
