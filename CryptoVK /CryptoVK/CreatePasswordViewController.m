//
//  CreatePasswordViewController.m
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "CreatePasswordViewController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Password+CoreDataClass.h"

@interface CreatePasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation CreatePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.passwordTextField becomeFirstResponder];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didChangeEditing:(UITextField *)sender {
    if ([sender.text length]==4){
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
        Password *password=[[Password alloc] initWithContext:context];
        password.numberText=sender.text;
        [context save:nil];
        [self performSegueWithIdentifier:@"FromCreatingPassword" sender:nil];
    }
}

@end



