//
//  AuthenticationViewController.m
//  CryptoVK
//
//  Created by 1 on 06.07.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "AuthenticationViewController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Token+CoreDataClass.h"
#import <LocalAuthentication/LocalAuthentication.h>


@interface AuthenticationViewController () 

@end

@implementation AuthenticationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *request = [Token fetchRequest];
    NSArray <Token*>*tokens=[[context executeFetchRequest:request error:nil] copy];
    Token *token=[tokens firstObject];
   
    //проверяем существует ли токен, если существует значит пользователь уже регистрировался и вводил пароль, тогда запрашиваем его, иначе - создаем новый пароль
    if (token){
        [self authentication];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"ToCreatePassword" sender:nil];
        });
     }

}
- (void) authentication{
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    
    //если есть возможность осуществляем вход с помощью  TouchID, иначе запрашиваем пароль обычным способом
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:@"Use your fingers!"
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self performSegueWithIdentifier:@"success" sender:nil];
                                    });
                                } else { //если проблемы с TouchID то запрашиваем пароль обычным способом
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self performSegueWithIdentifier:@"ToInputPassword" sender:nil];
                                    });
                                }
                            }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"ToInputPassword" sender:nil];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
