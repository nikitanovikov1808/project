//
//  VKDialogViewController.h
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "ViewController.h"
@class User;

@interface VKDialogViewController : ViewController

@property (strong, nonatomic) User *friend;

@end
