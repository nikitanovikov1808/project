//
//  VKDialogDataSource.h
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;

@interface VKDialogDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, copy) NSMutableArray *messages;
@property (strong, nonatomic) User *friend;

- (instancetype)initWithActionHandler:(id) actionHandler;


@end
