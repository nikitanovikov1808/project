//
//  VKDialogViewController.m
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKDialogViewController.h"
#import "SessionManager.h"
#import "User.h"
#import "VKDialogDataSource.h"
#import "VKDialogVCDelegate.h"
#import "ActionHandlerForVKDialog.h"
#import "FriendTableViewCell.h"
#import "EncryptionAlgorithm.h"

static const NSInteger messagesInRequest = 50;

@interface VKDialogViewController () <ActionHandlerForVKDialog,UITextFieldDelegate>

@property (nonatomic, strong) VKDialogDataSource *dataSource;
@property (nonatomic, strong) VKDialogVCDelegate *delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL isObtaining;
@property (weak, nonatomic) IBOutlet UITextField *messageTextField;
@property (weak, nonatomic) IBOutlet UISwitch *safeSwither;
@property (weak, nonatomic) IBOutlet UILabel *safeLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation VKDialogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isObtaining = NO;
    [self configurateDelegates];
    [self.activityIndicator startAnimating];
    [self obtainMessages];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.navigationItem.title=self.friend.firstName;

    
    // Do any additional setup after loading the view.
}
-(void) dealloc{
    [self.messageTextField resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) configurateDelegates{
    self.dataSource = [[VKDialogDataSource alloc] initWithActionHandler:self];
    self.dataSource.friend=self.friend;
    self.tableView.dataSource = self.dataSource;
    self.delegate = [[VKDialogVCDelegate alloc] initWithActionHandler:self];
    self.tableView.delegate=self.delegate;
    self.messageTextField.delegate = self;
}


-(void) obtainMessages{
    __weak VKDialogViewController *weakSelf = self;
    [[SessionManager sharedManager] getDialogWithFriend:self.friend.ID
                                                 offset:[self.dataSource.messages count]
                                                  count:messagesInRequest
                                              onSuccess:^(NSArray *messages) {
                                                  [weakSelf.dataSource.messages addObjectsFromArray:messages];
                                                  [weakSelf.tableView reloadData];
                                                  if (messages.count>0){
                                                      [weakSelf scrollToTopAtIndexPath:messages.count-1];
                                                  }
                                                  weakSelf.isObtaining = NO;
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [weakSelf.activityIndicator stopAnimating];
                                                  });
                                              }
                                              onFailure:^(NSError *error) {
                                                  NSLog(@"error = %@", [error localizedDescription] );
                                              }];
}

- (void) scrollToTopAtIndexPath:(NSUInteger)index{
    if (self.dataSource.messages.count > index+1){
        index++;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

#pragma mark - ActionHandlerForVKDialog Implementation
- (void)didScrollToTop{
    if (self.isObtaining) {
        return;
    }
    if (self.dataSource.messages.count > 0) {
        [self.activityIndicator startAnimating];
        [self obtainMessages];
    }
    else
        return;
    self.isObtaining = YES;
}

- (void)handleSelectionAtIndexPath:(NSIndexPath *)indexPath {
    [self.messageTextField resignFirstResponder];
}

- (CGFloat) heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger index=(self.dataSource.messages.count-1)-indexPath.row;
    return [FriendTableViewCell heightForText:self.dataSource.messages[index]];
}


#pragma mark - UITextFieldDelegate Implementation

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    NSString *message=self.messageTextField.text;
    if (self.safeSwither.isOn){
        EncryptionAlgorithm *cryptionAlgorithm=[EncryptionAlgorithm new];
        message=[cryptionAlgorithm encryptMessage:message];
    }
    __weak VKDialogViewController *weakSelf = self;
    [[SessionManager sharedManager] sendMessage:message
                                       toFriend:self.friend.ID
                                      onSuccess:^(NSString *success) {
                                          if ([success isEqualToString:@"success"]){
                                              [weakSelf.dataSource.messages removeAllObjects];
                                              [weakSelf obtainMessages];
                                          }
                                      }
                                      onFailure:^(NSError *error) {
                                          NSLog(@"error = %@", [error localizedDescription] );
                                      }];
    self.messageTextField.text=nil;
    return YES;
}


#pragma mark - keyboardNotification

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardBounds;
    [keyboardBoundsValue getValue:&keyboardBounds];
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - keyboardBounds.size.height);
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardBounds;
    [keyboardBoundsValue getValue:&keyboardBounds];
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + keyboardBounds.size.height);
}
#pragma mark - Action
- (IBAction)switherChangedValue:(UISwitch *)sender {
    if (sender.isOn){
        self.messageTextField.keyboardType=UIKeyboardTypeASCIICapable;
        self.safeLabel.backgroundColor=[UIColor cyanColor];
    }
    else{
        self.messageTextField.keyboardType=UIKeyboardTypeDefault;
        self.safeLabel.backgroundColor=[UIColor whiteColor];
    }
    if (    [self.messageTextField resignFirstResponder])
                [self.messageTextField becomeFirstResponder];
}

@end
