//
//  FriendTableViewCell.h
//  CryptoVK
//
//  Created by 1 on 27.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

+ (CGFloat) heightForText:(NSString*) text;

@end
