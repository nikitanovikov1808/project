//
//  VKDialogVCDelegate.h
//  CryptoVK
//
//  Created by iOS-School-2 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionHandlerForVKDialog;


@interface VKDialogVCDelegate : NSObject <UITableViewDelegate>

- (instancetype)initWithActionHandler:(id <ActionHandlerForVKDialog>)actionHandler;

@end
