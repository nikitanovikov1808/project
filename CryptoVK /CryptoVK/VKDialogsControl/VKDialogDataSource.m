//
//  VKDialogDataSource.m
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKDialogDataSource.h"
#import "UserTableViewCell.h"
#import "FriendTableViewCell.h"
#import "UserTableViewCell.h"
#import "User.h"

static  NSString *prefixForIDUser = @"<-userID,message->";
static  NSString *friendCell = @"FriendCellID";
static  NSString *userCell = @"UserCellID";



@interface VKDialogDataSource()

@property (nonatomic, weak) id  actionHandler;

@end

@implementation VKDialogDataSource

- (instancetype)initWithActionHandler:(id)actionHandler {
    self = [super init];
    if (self) {
        _actionHandler = actionHandler;
        _messages=[@[] mutableCopy];
        _friend=[User new];

    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.messages) {
        return self.messages.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index=(self.messages.count-1)-indexPath.row;
    id  obj =self.messages[index];
    if([obj isKindOfClass:[NSString class]]){
        NSString *query=obj;
        NSArray* array = [query componentsSeparatedByString:prefixForIDUser];
        if ([array count] > 1) {
            NSString *message = [array lastObject];
            NSString *userID=[array firstObject];
            if (userID.integerValue == self.friend.ID.integerValue){
                FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:friendCell
                                                                            forIndexPath:indexPath];
                cell.messageLabel.text=message;
                return cell;
            }
            else{
                UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:userCell
                                                                            forIndexPath:indexPath];
                cell.messageLabel.text=message;
                return cell;
            }
        }
    }
    return nil;

}

@end
