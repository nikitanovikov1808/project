//
//  VKDialogVCDelegate.m
//  CryptoVK
//
//  Created by iOS-School-2 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKDialogVCDelegate.h"
#import "ActionHandlerForVKDialog.h"

@interface VKDialogVCDelegate()

@property (nonatomic, weak) id <ActionHandlerForVKDialog> actionHandler;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation VKDialogVCDelegate

- (instancetype)initWithActionHandler:(id <ActionHandlerForVKDialog>)actionHandler {
    self = [super init];
    if (self) {
        _actionHandler = actionHandler;
    }
    return self;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger offset=-70;
    if (scrollView.contentOffset.y < offset) {
        [self.timer invalidate];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                      target:self
                                                    selector:@selector(handleDidScrollToTop)
                                                    userInfo:nil repeats:NO];

    }
}

- (void)handleDidScrollToTop{
    [self.actionHandler didScrollToTop];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     return [self.actionHandler heightForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.actionHandler) {
        [self.actionHandler handleSelectionAtIndexPath:indexPath];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
