//
//  ActionHandlerForVKDialog.h
//  CryptoVK
//
//  Created by 1 on 27.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ActionHandlerForVKDialog <NSObject>

- (void)didScrollToTop;

- (CGFloat) heightForRowAtIndexPath:(NSIndexPath *)indexPath;

- (void)handleSelectionAtIndexPath:(NSIndexPath *)indexPath;

@end
