//
//  VKFriendsVCDataSource.h
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface VKFriendsVCDataSource : NSObject <UITableViewDataSource>


@property (nonatomic, copy) NSMutableArray *friends;

- (instancetype)initWithActionHandler:(id) actionHandler;


@end
