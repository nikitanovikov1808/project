//
//  VKFriendsVCDelegate.h
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ActionHandlerForVKFriends;


@interface VKFriendsVCDelegate : NSObject <UITableViewDelegate>

- (instancetype)initWithActionHandler:(id <ActionHandlerForVKFriends>)actionHandler;

@end
