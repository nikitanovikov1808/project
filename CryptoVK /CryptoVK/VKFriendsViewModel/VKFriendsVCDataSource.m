//
//  VKFriendsVCDataSource.m
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKFriendsVCDataSource.h"
#import "UIImageView+WebCache.h"
#import "User.h"
#import "UIImageView+AFNetworking.h"




@interface VKFriendsVCDataSource()

@property (nonatomic, weak) id  actionHandler;

@end

@implementation VKFriendsVCDataSource

- (instancetype)initWithActionHandler:(id)actionHandler {
    self = [super init];
    if (self) {
        _actionHandler = actionHandler;
        _friends=[@[] mutableCopy];
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (self.friends) {
        return self.friends.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseID"
                                                            forIndexPath:indexPath];
    id  obj =self.friends[indexPath.row];
    if([obj isKindOfClass:[User class]]){
        User *user=obj;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",user.firstName, user.lastName];
        UIImage *noPhotoImage=[UIImage imageNamed:@"noPhotoUserAvatar.jpg"];
        [cell.imageView sd_setImageWithURL:user.imageURL placeholderImage:noPhotoImage];
    }

    
    return cell;
}

@end
