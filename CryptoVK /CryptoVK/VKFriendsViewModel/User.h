//
//  User.h
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AccessToken;

@interface User : NSObject

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSURL* imageURL;
@property (strong, nonatomic) NSString* ID;


- (id) initWithServerResponse:(NSDictionary*) responseObject;

@end
