//
//  User.m
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "User.h"

@implementation User

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        
        NSString* urlString = [responseObject objectForKey:@"photo_50"];
        
        if (urlString) {
            self.imageURL = [NSURL URLWithString:urlString];
        }
        self.ID=[responseObject objectForKey:@"uid"];
    }
    return self;
}


@end
