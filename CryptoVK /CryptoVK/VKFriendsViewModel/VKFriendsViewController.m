//
//  VKFriendsViewController.m
//  CryptoVK
//
//  Created by 1 on 25.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKFriendsViewController.h"
#import "User.h"
#import "SessionManager.h"
#import "VKFriendsVCDataSource.h"
#import "VKFriendsVCDelegate.h"
#import "ActionHandlerForVKFriends.h"
#import "VKDialogViewController.h"

static const NSInteger friendsInRequest = 30;


@interface VKFriendsViewController () <ActionHandlerForVKFriends,UISearchBarDelegate>

@property (strong, nonatomic) User * authorizedUser;
@property (nonatomic, strong) VKFriendsVCDataSource *dataSource;
@property (nonatomic, strong) VKFriendsVCDelegate *delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSTimer *timer;
@property (assign, nonatomic) BOOL isObtaining;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation VKFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isObtaining=NO;
    [self configurateDelegates];
    [self.activityIndicator startAnimating];
    [self authorize];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) configurateDelegates{
    self.dataSource = [[VKFriendsVCDataSource alloc] initWithActionHandler:self];
    self.tableView.dataSource = self.dataSource;
    self.delegate = [[VKFriendsVCDelegate alloc] initWithActionHandler:self];
    self.tableView.delegate=self.delegate;
    self.searchBar.delegate=self;
}

- (void) authorize{
    __weak VKFriendsViewController *weakSelf = self;
    [[SessionManager sharedManager] authorizeUser:^(User *user) {
        [weakSelf configurateAuthorizedUser:user];
        [weakSelf obtainFriends];
    }];
   }

- (void) configurateAuthorizedUser:(User *) user{
    self.authorizedUser=user;
    __weak VKFriendsViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue
                   (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       NSData *data=[NSData dataWithContentsOfURL:user.imageURL];
                       UIImage *image=[UIImage imageWithData:data];
                       UIImageView *imageView=[[UIImageView alloc] initWithImage: image];
                       imageView.frame = CGRectMake(0, 0, 40, 40);
                       imageView.layer.cornerRadius = 20.0;
                       imageView.layer.masksToBounds = YES;
                       dispatch_async(dispatch_get_main_queue(), ^{
                           weakSelf.navigationItem.titleView = imageView;
                       }) ;
                   });
}

- (void) obtainFriends{
    __weak VKFriendsViewController *weakSelf = self;
    [[SessionManager sharedManager] getFriendsWithOffset:[self.dataSource.friends count]
                                                   count:friendsInRequest
                                               onSuccess:^(NSArray *friends) {
                                                   [weakSelf.dataSource.friends addObjectsFromArray:friends];
                                                   weakSelf.isObtaining = NO;
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [weakSelf.activityIndicator stopAnimating];
                                                       [weakSelf.tableView reloadData];
                                                   });
                                               }
                                               onFailure:^(NSError *error) {
                                                   NSLog(@"error = %@", [error localizedDescription] );
                                               }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ToDialogFromFriend"]) {
        [self.searchBar resignFirstResponder];
        VKDialogViewController *viewController = segue.destinationViewController;
        NSUInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
        User *user = self.dataSource.friends[selectedIndex];
        viewController.friend = user;
    }
}


#pragma mark - ActionHandlerForVKFriends Implementation

- (void)didScrollToBottom{
    if (self.isObtaining) {
        return;
    }
    if (self.dataSource.friends.count > 0) {
        [self.activityIndicator startAnimating];
        [self obtainFriends];
    }
    else
        return;
    self.isObtaining = YES;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(search)
                                                userInfo:nil repeats:NO];
}

- (void) search{
    if (self.searchBar.text.length < 2) {
        return;
    }
    self.isObtaining = YES;
    [self.dataSource.friends removeAllObjects];
    [self.tableView reloadData];
    [self.activityIndicator startAnimating];
    __weak VKFriendsViewController *weakSelf = self;
    [[SessionManager sharedManager] searchFriend:self.searchBar.text
                                               onSuccess:^(NSArray *friends) {
                                                   [weakSelf.dataSource.friends addObjectsFromArray:friends];
                                                   [weakSelf.tableView reloadData];
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [weakSelf.activityIndicator stopAnimating];
                                                   });
                                               }
                                               onFailure:^(NSError *error) {
                                                   NSLog(@"error = %@", [error localizedDescription] );
                                               }];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    self.isObtaining = YES;
    [self.dataSource.friends removeAllObjects];
    [self.tableView reloadData];
    [self.activityIndicator startAnimating];
    __weak VKFriendsViewController *weakSelf = self;
    [[SessionManager sharedManager] searchFriend:self.searchBar.text
                                       onSuccess:^(NSArray *friends) {
                                           [weakSelf.dataSource.friends addObjectsFromArray:friends];
                                           [weakSelf.tableView reloadData];
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [weakSelf.activityIndicator stopAnimating];
                                           });
                                       }
                                       onFailure:^(NSError *error) {
                                           NSLog(@"error = %@", [error localizedDescription] );
                                       }];

    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [self.dataSource.friends removeAllObjects];
    [self obtainFriends];
    self.searchBar.text=nil;
    self.isObtaining = NO;
    
}
#pragma mark - UIBarButtonItem Action


- (IBAction) exit:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Выход" message:@"Вы действительно хотите выйти из своего профиля Вконтакте и из приложения CryptoVK?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *exitButton = [UIAlertAction actionWithTitle:@"Выйти" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[SessionManager sharedManager] exit];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:exitButton];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
