//
//  VKFriendsVCDelegate.m
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "VKFriendsVCDelegate.h"
#import "ActionHandlerForVKFriends.h"

@interface VKFriendsVCDelegate()

@property (nonatomic, weak) id <ActionHandlerForVKFriends> actionHandler;

@end

@implementation VKFriendsVCDelegate

- (instancetype)initWithActionHandler:(id <ActionHandlerForVKFriends>)actionHandler {
    self = [super init];
    if (self) {
        _actionHandler = actionHandler;
    }
    return self;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > scrollView.contentSize.height - CGRectGetHeight(scrollView.frame)) {
        [self.actionHandler didScrollToBottom];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
