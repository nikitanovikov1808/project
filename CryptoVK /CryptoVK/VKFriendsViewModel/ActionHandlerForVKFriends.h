//
//  ActionHandler.h
//  CryptoVK
//
//  Created by 1 on 26.06.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ActionHandlerForVKFriends <NSObject>

- (void)didScrollToBottom;

@end

